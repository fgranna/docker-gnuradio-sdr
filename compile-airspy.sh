#!/bin/bash

cnt=`grep 'processor.*:' /proc/cpuinfo|wc -l`


if [ $cnt -lt 1 ]
then
  cnt=1
fi

git clone https://github.com/airspy/host.git

mkdir /opt/host/build

cd /opt/host/build

cmake ../ -DINSTALL_UDEV_RULES=ON

make -j$cnt

make install

ldconfig

rm -rf /opt/host
