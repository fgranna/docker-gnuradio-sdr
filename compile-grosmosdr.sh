#!/bin/bash

cnt=`grep 'processor.*:' /proc/cpuinfo|wc -l`


if [ $cnt -lt 1 ]
then
  cnt=1
fi

git clone https://github.com/osmocom/gr-osmosdr.git

mkdir /opt/gr-osmosdr/build

cd /opt/gr-osmosdr/build

cmake ../

make -j$cnt

make install

ldconfig

rm -rf /opt/gr-osmosdr
