#!/bin/bash

cnt=`grep 'processor.*:' /proc/cpuinfo|wc -l`


if [ $cnt -lt 1 ]
then
  cnt=1
fi

git clone https://github.com/pothosware/SoapySDR.git

mkdir /opt/SoapySDR/build

cd /opt/SoapySDR/build

cmake ../

make -j$cnt

make install

ldconfig

rm -rf /opt/SoapySDR
