#https://gnuradio.org/redmine/projects/gnuradio/wiki/UbuntuInstall

FROM sysrun/gnuradio:latest

MAINTAINER Frederik Granna

RUN apt-get update && \
    apt-get install -y libusb-1.0-0-dev --no-install-recommends && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /opt

COPY compile-libsdr.sh /opt/compile-libsdr.sh
RUN ./compile-libsdr.sh && rm compile-libsdr.sh

COPY compile-airspy.sh /opt/compile-airspy.sh
RUN ./compile-airspy.sh && rm compile-airspy.sh

COPY compile-griqbal.sh /opt/compile-griqbal.sh
RUN ./compile-griqbal.sh && rm compile-griqbal.sh

COPY compile-soapysdr.sh /opt/compile-soapysdr.sh
RUN ./compile-soapysdr.sh && rm compile-soapysdr.sh

COPY compile-grosmosdr.sh /opt/compile-grosmosdr.sh
RUN ./compile-grosmosdr.sh && rm compile-grosmosdr.sh
