#!/bin/bash

cnt=`grep 'processor.*:' /proc/cpuinfo|wc -l`


if [ $cnt -lt 1 ]
then
  cnt=1
fi

git clone https://github.com/steve-m/librtlsdr.git

mkdir /opt/librtlsdr/build

cd /opt/librtlsdr/build

cmake ../ -DINSTALL_UDEV_RULES=ON -DDETACH_KERNEL_DRIVER=ON

make -j$cnt

make install

ldconfig

rm -rf /opt/librtlsdr
