#!/bin/bash

cnt=`grep 'processor.*:' /proc/cpuinfo|wc -l`


if [ $cnt -lt 1 ]
then
  cnt=1
fi

git clone --recursive git://git.osmocom.org/gr-iqbal.git

mkdir /opt/gr-iqbal/build

cd /opt/gr-iqbal/build

cmake ../

make -j$cnt

make install

ldconfig

rm -rf /opt/gr-iqbal
